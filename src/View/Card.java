package View;

import java.util.List;

public class Card {
        public Type type;
        public String color;
        public int Num;

        public static final String RED = "\033[0;31m";     // RED
        public static final String GREEN = "\033[0;32m";   // GREEN
        public static final String YELLOW = "\033[0;33m";  // YELLOW
        public static final String BLUE = "\033[0;34m";    // BLUE
        public static final String RESET = "\033[0m";
        public static final String WILD = "\033[0;36m";   // CYAN

        public Card(Type type, String color){
            this.color = color;
            this.type = type;
        }

        public Card(Type type, String color, int Num){
            this.color = color;
            this.type = type;
            this.Num = Num;
        }

        public Card(Type type){
            this.type = type;
            this.color = WILD;
        }
    @Override
    public String toString() {
            if(this.type== Type.Num){
                return this.color+"-------\n"+
                        "|     |\n"+
                        "|  "+this.Num+"  |\n"+
                        "|     |\n"+
                        "-------"+RESET;
            }
            if (this.type== Type.Draw2){
                return this.color+"-------\n"+
                        "|     |\n"+
                        "| D2  |\n"+
                        "|     |\n"+
                        "-------"+RESET;
            }
            if(this.type== Type.WildDraw4){
                return this.color+"-------\n"+
                        "|     |\n"+
                        "| WD4 |\n"+
                        "|     |\n"+
                        "-------"+RESET;
            }
            if(this.type== Type.Wild){
                return this.color+"-------\n"+
                        "|     |\n"+
                        "|  W  |\n"+
                        "|     |\n"+
                        "-------"+RESET;
            }
        if(this.type== Type.Skip){
            return this.color+"-------\n"+
                    "|     |\n"+
                    "|  S  |\n"+
                    "|     |\n"+
                    "-------"+RESET;
        }
        if(this.type== Type.Reverse){
            return this.color+"-------\n"+
                    "|     |\n"+
                    "|  R  |\n"+
                    "|     |\n"+
                    "-------"+RESET;
        }
        return "";
    }

    public static String printCards(List<Card> list){
            String str = "";
            for(Card card : list){
              str += card.color+"-------  "+Card.RESET;
          }
            str+="\n";

        for(Card card : list){
            str += card.color+"|     |  "+Card.RESET;
        }
        str+="\n";


        for(Card card : list){
            if(card.type== Type.Num){
                str += card.color+"|  "+card.Num+"  |  "+Card.RESET;
            }
            if(card.type== Type.Skip){
                str += card.color+"|  S  |  "+Card.RESET;
            }
            if(card.type== Type.Wild){
                str += card.color+"|  W  |  "+Card.RESET;
            }
            if(card.type== Type.Reverse){
                str += card.color+"|  R  |  "+Card.RESET;
            }
            if(card.type== Type.WildDraw4){
                str += card.color+"| WD4 |  "+Card.RESET;
            }
            if(card.type== Type.Draw2){
                str += card.color+"| D2  |  "+Card.RESET;
            }
        }
        str+="\n";

        for(Card card : list){
            str += card.color+"|     |  "+Card.RESET;
        }
        str+="\n";

        for(Card card : list){
            str += card.color+"-------  "+Card.RESET;
        }
        str+="\n";

        return str;
    }
//    public static void main(String[] args) {
//        View.Card card = new View.Card(Type.Skip, Color.G,1);
//        View.Card card1 = new View.Card(Type.Reverse, Color.B,1);
//        View.Card card2 = new View.Card(Type.WildDraw4, Color.Y,1);
//        View.Card card3 = new View.Card(Type.Skip, Color.Y,1);
//        View.Card card4= new View.Card(Type.Skip, Color.R,1);
//        View.Card card5 = new View.Card(Type.Skip, Color.G,1);
//        View.Card card6 = new View.Card(Type.Skip, Color.G,1);
//        List<View.Card> list = new ArrayList<>();
//        list.add(card);
//        list.add(card1);
//        list.add(card2);
//        list.add(card3);
//        list.add(card4);
//        list.add(card5);
//        list.add(card6);
//        System.out.println(View.Card.printCards(list));
//    }
}
