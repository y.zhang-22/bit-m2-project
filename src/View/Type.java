package View;

public enum Type {
    Num,
    Skip,
    Wild,
    WildDraw4,
    Draw2,
    Reverse
}