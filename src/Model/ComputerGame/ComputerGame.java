package Model.ComputerGame;

import View.Card;
import View.Type;
import Exception.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ComputerGame {
    private Card Discard;
    private List<Card> cardList;
    private List<Card> playerCards;
    private ComputerPlayer computerPlayer;
    private Boolean playerTurn = true;

    public ComputerGame (){
        cardList = new ArrayList<Card>();
        shuffle(cardList);
    }

    public void setComputerPlayer(ComputerPlayer computerPlayer){
        this.computerPlayer = computerPlayer;
    }

    public List<Card> getCards(){
        return cardList;
    }

    public List<Card> getPlayerCards(){
        return playerCards;
    }

    public void setDiscard(Card card){
        this.Discard = card;
    }

    public Card getDiscard(){
        return Discard;
    }

    /**
     * start the game
     */
    public void Start(){
        playerCards = new ArrayList<Card>();
        this.computerPlayer = new ComputerPlayer();

        for(int j = 0; j<7 ;j++){
            playerCards.add(cardList.get(0));
            cardList.remove(0);
        }

        for(int j = 0; j<7 ;j++){
            computerPlayer.addCards(cardList.get(0));
            cardList.remove(0);
        }

        System.out.println("Game starts");
        System.out.println(Discard);
        System.out.println(Card.printCards(playerCards));
        System.out.println("Your turn");
        outerLoop : while (true){
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            String[] commands = command.split(" ");
            if(commands.length!=2 && !commands[0].equals("help") && !commands[0].equals("draw") ){
                System.out.println("wrong command");
                continue;
            }
            switch (commands[0]){
                case "play":
                    if (playerTurn){
                        int cardIndex = Integer.parseInt(commands[1]);
                        if(cardIndex > playerCards.size() || cardIndex <=0){
                            System.out.println("wrong command");
                            continue;
                        }
                        Card card = this.playerCards.get(cardIndex-1);
                        try {
                            if(card.type== Type.Num){
                                if(!this.playNum(card)){
                                    continue;
                                }
                            } else if(card.type== Type.Reverse){
                                if(!this.playReverse(card)){
                                    continue;
                                }
                            } else if(card.type== Type.Wild){
                                this.playWild(card);
                            } else if(card.type== Type.WildDraw4){
                                this.playWildDraw4(card);
                            } else if(card.type== Type.Skip){
                                if(!this.Skip(card)){
                                    System.out.println("Your turn");
                                    System.out.println(Card.printCards(playerCards));
                                    continue outerLoop;
                                }
                            } else if(card.type== Type.Draw2){
                                if(!this.playDraw2(card)){
                                    continue;
                                }
                            }
                        } catch (WrongCardException e){
                            System.out.println("Invalid action");
                            System.out.println("Your turn");
                            System.out.println(Card.printCards(playerCards));
                            continue outerLoop;
                        }
                        if(playerCards.size() == 0){
                            System.out.println("You win!");
                            System.exit(0);
                        }
                        this.CPMovement();
                        if(computerPlayer.AICardList.size() == 0){
                            System.out.println("Computer player wins!");
                            System.exit(0);
                        }
                        label : System.out.println("your turn");
                        System.out.println(Card.printCards(playerCards));

                    } else {
                        System.out.println("not your turn");
                    }
                    break;

                case "draw":
                    if(playerTurn){
                        playerCards.add(cardList.get(0));
                        cardList.remove(0);
                        playerTurn = false;
                        this.CPMovement();
                        System.out.println("your turn");
                        System.out.println(Card.printCards(playerCards));
                    } else {
                        System.out.println("not your turn");
                    }
                    break;
            }
        }
    }

    /**
     * reset the cardList and assign cards to player and Computer Player
     * @require cardList = null
     */
    public void shuffle(List<Card> cardList){
        cardList.add(new Card(Type.Num,Card.RED,0));
        cardList.add(new Card(Type.Num,Card.YELLOW,0));
        cardList.add(new Card(Type.Num,Card.BLUE,0));
        cardList.add(new Card(Type.Num,Card.GREEN,0));
        for(int i = 1 ; i<10; i++){
            cardList.add(new Card(Type.Num,Card.RED,i));
            cardList.add(new Card(Type.Num,Card.YELLOW,i));
            cardList.add(new Card(Type.Num,Card.BLUE,i));
            cardList.add(new Card(Type.Num,Card.GREEN,i));
            cardList.add(new Card(Type.Num,Card.RED,i));
            cardList.add(new Card(Type.Num,Card.YELLOW,i));
            cardList.add(new Card(Type.Num,Card.BLUE,i));
            cardList.add(new Card(Type.Num,Card.GREEN,i));
        }
        cardList.add(new Card(Type.Draw2,Card.RED));
        cardList.add(new Card(Type.Draw2,Card.YELLOW));
        cardList.add(new Card(Type.Draw2,Card.BLUE));
        cardList.add(new Card(Type.Draw2,Card.GREEN));
        cardList.add(new Card(Type.Draw2,Card.RED));
        cardList.add(new Card(Type.Draw2,Card.YELLOW));
        cardList.add(new Card(Type.Draw2,Card.BLUE));
        cardList.add(new Card(Type.Draw2,Card.GREEN));

        cardList.add(new Card(Type.Reverse,Card.RED));
        cardList.add(new Card(Type.Reverse,Card.YELLOW));
        cardList.add(new Card(Type.Reverse,Card.BLUE));
        cardList.add(new Card(Type.Reverse,Card.GREEN));
        cardList.add(new Card(Type.Reverse,Card.RED));
        cardList.add(new Card(Type.Reverse,Card.YELLOW));
        cardList.add(new Card(Type.Reverse,Card.BLUE));
        cardList.add(new Card(Type.Reverse,Card.GREEN));

        cardList.add(new Card(Type.Skip,Card.RED));
        cardList.add(new Card(Type.Skip,Card.YELLOW));
        cardList.add(new Card(Type.Skip,Card.BLUE));
        cardList.add(new Card(Type.Skip,Card.GREEN));
        cardList.add(new Card(Type.Skip,Card.RED));
        cardList.add(new Card(Type.Skip,Card.YELLOW));
        cardList.add(new Card(Type.Skip,Card.BLUE));
        cardList.add(new Card(Type.Skip,Card.GREEN));

        cardList.add(new Card(Type.WildDraw4));
        cardList.add(new Card(Type.WildDraw4));
        cardList.add(new Card(Type.WildDraw4));
        cardList.add(new Card(Type.WildDraw4));

        cardList.add(new Card(Type.Wild));
        cardList.add(new Card(Type.Wild));
        cardList.add(new Card(Type.Wild));
        cardList.add(new Card(Type.Wild));

        Collections.shuffle(cardList);

        Discard = cardList.get(0);
        cardList.remove(0);
    }

    /**
     * play a Num card, if the card matches the Discard, replaces the Discard and return true
     * @require card != Null, card.Type == Type.Num
     */
    public Boolean playNum(Card card) throws WrongCardException {
        if (!card.color.equals(this.Discard.color)  && !this.Discard.color.equals(Card.WILD)){
            throw new WrongCardException("Invalid action");
        } else {
            this.Discard = card;
            playerCards.remove(card);
            System.out.println(card);
            playerTurn = false;
            return true;
        }
    }

    /**
     * play a Reverse card, if the card matches the Discard, replaces the Discard and return true
     * @require card != Null, card.Type == Type.Reverse
     */
    public Boolean playReverse(Card card) throws WrongCardException {
        if (!card.color.equals(this.Discard.color)  && !this.Discard.color.equals(Card.WILD)){
            throw new WrongCardException("Invalid action");
        } else {
            this.Discard = card;
            playerCards.remove(card);
            System.out.println(card);
            playerTurn = false;
            return true;
        }
    }

    /**
     * play a Wild card
     * @require card != Null, card.Type == Type.Wild
     */
    public void playWild(Card card){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Pick a color. R/Y/B/G");
        while (true){
            String color = scanner.nextLine();
            if(color.equals("R")){
                card.color = Card.RED;
                break;
            } else if(color.equals("Y")){
                card.color = Card.YELLOW;
                break;
            } else if(color.equals("G")){
                card.color = Card.GREEN;
                break;
            } else if(color.equals("B")){
                card.color = Card.BLUE;
                break;
            }
            System.out.println("wrong command");
        }
        Discard = card;
        playerCards.remove(card);
        playerTurn = false;
        System.out.println(card);
    }

    /**
     * play a WildDraw4 card
     * @require card != Null, card.Type == Type.WildDraw4
     */
    public void playWildDraw4(Card card){
        this.playWild(card);
        computerPlayer.addCards(cardList.get(0));
        computerPlayer.addCards(cardList.get(1));
        computerPlayer.addCards(cardList.get(2));
        computerPlayer.addCards(cardList.get(3));
        cardList.remove(0);
        cardList.remove(0);
        cardList.remove(0);
        cardList.remove(0);
        playerTurn = false;
        System.out.println(card);
    }

    /**
     * play a Skip card
     * @require card != Null, card.Type == Type.Skip
     */
    public Boolean Skip(Card card) throws WrongCardException {
        if (!card.color.equals(this.Discard.color)  && !this.Discard.color.equals(Card.WILD)){
            throw new WrongCardException("Invalid action");

        } else {
            this.Discard = card;
            playerCards.remove(card);
            System.out.println(card);
            return true;
        }
    }

    /**
     * play a Draw2 card
     * @require card != Null, card.Type == Type.Draw2
     */
    public Boolean playDraw2(Card card) throws WrongCardException {
        if (!card.color.equals(this.Discard.color)  && !this.Discard.color.equals(Card.WILD)){
            throw new WrongCardException("Invalid action");

        } else {
            computerPlayer.addCards(cardList.get(0));
            computerPlayer.addCards(cardList.get(1));
            cardList.remove(0);
            cardList.remove(0);
            this.Discard = card;
            playerCards.remove(card);
            playerTurn = false;
            System.out.println(card);
            return true;
        }
    }

    /**
     * computer player makes a movement, and print it
     */
    public void CPMovement(){
        while (true){
            Card CPCard = computerPlayer.validMovement(Discard);
            if(CPCard==null){
                computerPlayer.addCards(cardList.get(0));
                cardList.remove(0);
                playerTurn = true;
                break;
            } else if(CPCard.type== Type.Num){
                Discard = CPCard;
            } else if(CPCard.type== Type.Reverse){
                Discard = CPCard;
            } else if(CPCard.type== Type.Wild){
                Discard = CPCard;
            } else if(CPCard.type== Type.WildDraw4){
                Discard = CPCard;
                playerCards.add(cardList.get(0));
                playerCards.add(cardList.get(1));
                playerCards.add(cardList.get(2));
                playerCards.add(cardList.get(3));
                cardList.remove(0);
                cardList.remove(0);
                cardList.remove(0);
                cardList.remove(0);
            } else if(CPCard.type== Type.Skip){
                Discard = CPCard;
                System.out.println(CPCard);
                continue;
            } else if(CPCard.type== Type.Draw2){
                Discard = CPCard;
                playerCards.add(cardList.get(0));
                playerCards.add(cardList.get(1));
                cardList.remove(0);
                cardList.remove(0);
            }
            playerTurn = true;
            System.out.println(CPCard);

            break;
        }
    }
}
