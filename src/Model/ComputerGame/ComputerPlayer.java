package Model.ComputerGame;

import View.Card;
import View.Type;

import java.util.ArrayList;
import java.util.List;

public class ComputerPlayer {
    public List<Card> AICardList= new ArrayList<Card>();

    public void addCards(Card card){
        AICardList.add(card);
    }

    public void removeCard(int index){
        AICardList.remove(index);
    }

    /**
     * computer player makes an action, if there is no card can be played, return null
     * @require discard != null
     */
    public Card validMovement(Card discard){
        Card cardToPlay = null;
        //first find is there a card has the same color with discard
        for(Card card : AICardList){
            if(card.type.equals(Type.Draw2) && (card.color.equals(discard.color) || discard.type.equals(Type.Draw2))){
                cardToPlay = card;
                AICardList.remove(card);
                return cardToPlay;
            } else if(card.type.equals(Type.Skip) && (card.color.equals(discard.color) || discard.type.equals(Type.Skip))) {
                cardToPlay = card;
                AICardList.remove(card);
                return cardToPlay;
            } else if(card.color.equals(discard.color)){
                cardToPlay = card;
                AICardList.remove(card);
                return cardToPlay;
            }
        }

        //if there is no same color card, then play Wild card
        for(Card card : AICardList){
            if(card.type.equals(Type.WildDraw4)){
                cardToPlay = card;
                AICardList.remove(card);
                cardToPlay.color = Card.RED;
                return cardToPlay;
            } else if(card.type.equals(Type.Wild)){
                cardToPlay = card;
                AICardList.remove(card);
                cardToPlay.color = Card.RED;
                return cardToPlay;
            }
        }

        return cardToPlay;
    }
}
