package Model.Server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import View.Card;
import View.Type;

public class Server {
    public static List<PlayerHandler> list = new ArrayList<PlayerHandler>();
    public static List<Card> cardList = new ArrayList<Card>();
    public static List<PlayerHandler> orderList = new ArrayList();
    public static Card Discard;
    public static int order ;
    public static boolean flag = false;
    public static String helpMessage="Draw 2 View.Card\n"+
            "When you play this card, the next person to play must draw 2 cards and forfeit his/her turn.\n"+
            "\n"+
            "Reverse View.Card\n"+
            "This card reverses direction of play. Play to the left now passes to the right, and vice versa.\n"+
            "\n"+
            "Skip View.Card\n"+
            "The next person in line to play after this card is played loses his/her turn and is \"skipped\".\n"+
            "\n"+
            "Wild View.Card\n"+
            "When you play this card, you may change the color being played to any color (including the current color) to continue play. You may play a Wild card even if you have another playable card in hand.\n"+
            "\n"+
            "Wild Draw 4 View.Card\n"+
            "This card allows you to call the next color played and requires the next player to pick 4 cards from the DRAW pile and forfeit his/her turn. However, there is a hitch!\n"+
            "\n"+
            "Command:\n"+
            "'play n' play the card with index n(start from 1)\n"+
            "\n"+
            "'help' print game rules and commands guide\n"+
            "\n"+
            "'chat ****' send message to other players\n"+
            "\n"+
            "'score' print all players' scores\n"+
            "\n"+
            "'draw' draw a card \n";


    /**
     * start the server
     * server != null, list.size() <= 10
     */
    public void startGame(){
        ServerSocket server = null;
        try {
            server = new ServerSocket(9999);
            while (true){
                Socket client = server.accept();

                //if the game has started, player can not join
                if(flag){
                    DataOutputStream dos = new DataOutputStream(client.getOutputStream());
                    dos.writeUTF("Sorry, the game has started");
                    continue;
                } else if(list.size()>10){
                    //if there are 10 players, player can not join
                    DataOutputStream dos = new DataOutputStream(client.getOutputStream());
                    dos.writeUTF("Sorry, the game is full");
                    continue;
                }

                PlayerHandler player = new PlayerHandler(client);

                list.add(player);

                if(list.size()!=1){
                    list.get(0).send(list.size()+" players in the game. Start? (enter 'yes' if you want to start)");

                }
                for (int i = 1 ; i<list.size()-1;i++){
                    list.get(i).send("new player joined");
                }
                new Thread(player).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * reset the cards list and assign players new cards
     * @ensure Discard != Null, Discard.Type != Type.WildDraw4
     */
    public static void shuffle(){
        flag = true;
        cardList.clear();
        Discard = null;
        order = 0;
        cardList.add(new Card(Type.Num,Card.RED,0));
        cardList.add(new Card(Type.Num,Card.YELLOW,0));
        cardList.add(new Card(Type.Num,Card.BLUE,0));
        cardList.add(new Card(Type.Num,Card.GREEN,0));
        for(int i = 1 ; i<10; i++){
            cardList.add(new Card(Type.Num,Card.RED,i));
            cardList.add(new Card(Type.Num,Card.YELLOW,i));
            cardList.add(new Card(Type.Num,Card.BLUE,i));
            cardList.add(new Card(Type.Num,Card.GREEN,i));
            cardList.add(new Card(Type.Num,Card.RED,i));
            cardList.add(new Card(Type.Num,Card.YELLOW,i));
            cardList.add(new Card(Type.Num,Card.BLUE,i));
            cardList.add(new Card(Type.Num,Card.GREEN,i));
        }
        cardList.add(new Card(Type.Draw2,Card.RED));
        cardList.add(new Card(Type.Draw2,Card.YELLOW));
        cardList.add(new Card(Type.Draw2,Card.BLUE));
        cardList.add(new Card(Type.Draw2,Card.GREEN));
        cardList.add(new Card(Type.Draw2,Card.RED));
        cardList.add(new Card(Type.Draw2,Card.YELLOW));
        cardList.add(new Card(Type.Draw2,Card.BLUE));
        cardList.add(new Card(Type.Draw2,Card.GREEN));

        cardList.add(new Card(Type.Reverse,Card.RED));
        cardList.add(new Card(Type.Reverse,Card.YELLOW));
        cardList.add(new Card(Type.Reverse,Card.BLUE));
        cardList.add(new Card(Type.Reverse,Card.GREEN));
        cardList.add(new Card(Type.Reverse,Card.RED));
        cardList.add(new Card(Type.Reverse,Card.YELLOW));
        cardList.add(new Card(Type.Reverse,Card.BLUE));
        cardList.add(new Card(Type.Reverse,Card.GREEN));

        cardList.add(new Card(Type.Skip,Card.RED));
        cardList.add(new Card(Type.Skip,Card.YELLOW));
        cardList.add(new Card(Type.Skip,Card.BLUE));
        cardList.add(new Card(Type.Skip,Card.GREEN));
        cardList.add(new Card(Type.Skip,Card.RED));
        cardList.add(new Card(Type.Skip,Card.YELLOW));
        cardList.add(new Card(Type.Skip,Card.BLUE));
        cardList.add(new Card(Type.Skip,Card.GREEN));

        cardList.add(new Card(Type.WildDraw4));
        cardList.add(new Card(Type.WildDraw4));
        cardList.add(new Card(Type.WildDraw4));
        cardList.add(new Card(Type.WildDraw4));

        cardList.add(new Card(Type.Wild));
        cardList.add(new Card(Type.Wild));
        cardList.add(new Card(Type.Wild));
        cardList.add(new Card(Type.Wild));


        Collections.shuffle(cardList);

        for(int i = 0; i<list.size();i++){
            list.get(i).cards.clear();
            for(int j = 0; j<7 ;j++){
                list.get(i).cards.add(cardList.get(0));
                cardList.remove(0);
            }
        }

        orderList = list;

        //check if the discard is special card
        while (true){
            Card card = cardList.get(0);
            cardList.remove(0);
            if(card.type.equals(Type.WildDraw4)){
                cardList.add(card);
                continue;
            }
            Server.Discard = card;
            break;
        }

        if(list.size()==0){
            return;
        }

        //if Discard is Draw 2 card, the player draws 2
        if(Discard.type.equals(Type.Draw2)){
            list.get(0).drawCards(2,list.get(0));
        } else if(Discard.type.equals(Type.Reverse)){ //if Discard is Reverse card, the order list reverses
            Collections.reverse(Server.orderList);
        } else if(Discard.type.equals(Type.Skip)){ //if Discard is Skip card, the player skips turns
            order = 1;
        }
    }
}
