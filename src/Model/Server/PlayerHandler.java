package Model.Server;

import View.Card;
import Exception.WrongCardException;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


import View.Type;
import org.junit.platform.commons.util.StringUtils;

public class PlayerHandler implements Runnable {
    private DataInputStream dis;
    private DataOutputStream dos;
    private boolean flag = true;
    public boolean start = false;
    public List<Card> cards = new ArrayList<Card>();
    public int score = 0;
    public String name;

    public PlayerHandler(){}
    public PlayerHandler(Socket client){
        try {
            dis = new DataInputStream(client.getInputStream());
            dos = new DataOutputStream(client.getOutputStream());
        } catch (IOException e) {
            flag = false;
            try {
                dis.close();
                dos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private String receive(){
        String str = "";
        try {
            str = dis.readUTF();
            //after host enter 'yes', start the game.
            if(this== Server.list.get(0) && str.equals("yes") &&this.start==false){
                Server.shuffle();
                for (PlayerHandler player : Server.orderList){
                    player.start=true;
                    player.send("Game start");
                    player.send(Server.Discard.toString());
                    player.send(Card.printCards(player.cards));
                }
                Server.orderList.get(Server.order).send("Your turn");
                return "";
            }
            if(start){
                    String[] arr = str.split(" ");
                    //check the command is valid
                    if(arr.length!=2 && !str.equals("help") && !str.equals("draw")  && !str.equals("score") && !arr[0].equals("chat")){
                        this.send("Wrong command");
                        return "";
                    }
                    switch (arr[0]){
                        case "help":
                            this.send(Server.helpMessage);
                            return "";
                        case "play":
                            int command = Integer.parseInt(arr[1]);
                            if(command <= 0 || command > this.cards.size()){
                                this.send("wrong command");
                                return "";
                            }
                            //check if it's this player's turn
                            if(Server.order != Server.orderList.indexOf(this)){
                                this.send("not your turn");
                                return "";
                            }
                            Card card = this.cards.get(command-1);
                            try {
                                if(card.type== Type.Num){
                                    this.playNum(card);
                                } else if(card.type== Type.Reverse){
                                    this.reverse(card);
                                } else if(card.type== Type.Wild){
                                    this.playWild(card);
                                } else if(card.type== Type.WildDraw4){
                                    this.playWildDraw4(card);
                                } else if(card.type== Type.Skip){
                                    this.Skip(card);
                                } else if(card.type== Type.Draw2){
                                    this.playDraw2(card);
                                }
                            } catch (WrongCardException exception){
                                this.send(exception.getMessage());
                            }
                            //after play a card, check if there is a winner
                            if(!this.checkWin()){
                                Server.orderList.get(Server.order).send("Your turn");
                                Server.orderList.get(Server.order).send(Card.printCards(Server.orderList.get(Server.order).cards));
                                return "";
                            }
                            //if there is a winner, start a new round, and add the score to the winner
                            this.sendAll("UNO");
                            this.sendAll("****************************************");
                            for(PlayerHandler player : Server.list){
                                if(player!=this){
                                    for(Card card1 : player.cards){
                                        if(card1.type==Type.Skip||card1.type==Type.Draw2||card1.type==Type.Reverse){
                                            this.score+=20;
                                        } else if(card1.type==Type.Wild || card1.type==Type.WildDraw4){
                                            this.score+=50;
                                        } else {
                                            this.score+=card1.Num;
                                        }
                                    }
                                }
                            }
                            //if the score is higher than 500, end the game
                            if(score>=500){
                                this.sendAll("winner is "+this.name);
                                System.exit(0);
                            }
                            this.restart();
                            return "";

                        case "draw":
                            this.DrawOneCard();
                            return "";

                        case "chat":
                            this.Chat(arr);
                            return "";

                        case "score" :
                            String scores = "";
                            for (PlayerHandler player : Server.list){
                                scores+=player.name+": "+player.score+"  ";
                            }
                            this.send(scores);
                            return "";

                        default:
                            this.send("wrong command");
                    }


            } else {
                return "";
            }
        } catch (IOException e) {
            flag = false;
            try {
                dis.close();
                dos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return str;
    }

    /**
     * This method will str to all players
     * @require str != null
     */
    public void sendAll(String str){
        List<PlayerHandler> list = Server.orderList;
        for (PlayerHandler other : list){
            other.send(str);
        }
    }

    /**
     * This method will str to others players
     * @require str != null
     */
    public void sendOthers(String str){
        List<PlayerHandler> list = Server.orderList;
        for (PlayerHandler other : list){
            if(other.equals(this)){
                continue;
            }
            other.send(str);
        }
    }

    /**
     * Send str to this player
     * @require str != null
     */
    public void send(String str){
        if(str != null && str.length() != 0){
            try {
                dos.writeUTF(str);
                dos.flush();
            } catch (IOException e) {
                flag = false;
                try {
                    dis.close();
                    dos.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }


    private void sendOther(){
        String str = this.receive();
        List<PlayerHandler> list = Server.orderList;
        for (PlayerHandler other : list){
            if(other==this){
                continue;
            }
            other.send(str);
        }
    }

    /**
     * draw a card
     * @require num>=0 && num <=9, player != null
     * @ensure player.cards,size() +1
     */
    public void drawCards(int num, PlayerHandler player){
        try {
            for(int i = 0 ; i < num ; i++ ){
                player.cards.add(Server.cardList.get(0));
                Server.cardList.remove(0);
            }
        } catch (Exception e){
            sendAll("TIE");
            this.sendAll("****************************************");
            this.restart();
        }

    }

    /**
     * play a Num card
     * @require card != null && card.Type == Num && card.color == Server.Discard.color
     */
    public void playNum(Card card) throws WrongCardException{
        if (!card.color.equals(Server.Discard.color)  && !Server.Discard.color.equals(Card.WILD)){
            throw new WrongCardException("Invalid action");
        }
        Server.Discard = card;
        this.cards.remove(card);
        this.sendAll(card.toString());
        if(Server.order==Server.orderList.size()-1){
            Server.order = 0;
        } else {
            Server.order++;
        }
    }

    /**
     * play a Reverse card
     * @require card != null && card.Type == Reverse && card.color == Server.Discard.color
     */
    public void reverse(Card card) throws WrongCardException{
        if (!card.color.equals(Server.Discard.color)  && !Server.Discard.color.equals(Card.WILD) && !Server.Discard.type.equals(Type.Reverse)){
            throw new WrongCardException("Invalid action");
        }
        Server.Discard = card;
        this.cards.remove(card);
        this.sendAll(card.toString());
        Collections.reverse(Server.orderList);
        if(Server.orderList.indexOf(this)==Server.orderList.size()-1){
            Server.order = 0;
        } else {
            Server.order = Server.orderList.indexOf(this)+1;
        }

    }

    /**
     * play a Wild card
     * @require card != null
     */
    public void playWild(Card card) throws IOException {
        this.send("Pick a color. R/Y/B/G");
        while (true){
            String color = dis.readUTF();
            if(color.equals("R")){
                card.color = Card.RED;
                break;
            } else if(color.equals("Y")){
                card.color = Card.YELLOW;
                break;
            } else if(color.equals("G")){
                card.color = Card.GREEN;
                break;
            } else if(color.equals("B")){
                card.color = Card.BLUE;
                break;
            }
            this.send("wrong command");
        }
        Server.Discard = card;
        this.cards.remove(card);
        this.sendAll(card.toString());
        if(Server.order==Server.orderList.size()-1){
            Server.order = 0;
        } else {
            Server.order++;
        }

    }

    /**
     * play Wild Draw 4 card
     * @require card != null
     */
    public void playWildDraw4(Card card) throws IOException, WrongCardException {
        for (Card c : cards){
            if(c.color.equals(Server.Discard.color)){
                throw new WrongCardException("Invalid action");
            }
        }
        this.send("Pick a color. R/Y/B/G");
        while (true){
            String color = dis.readUTF();
            if(color.equals("R")){
                card.color = Card.RED;
                break;
            } else if(color.equals("Y")){
                card.color = Card.YELLOW;
                break;
            } else if(color.equals("G")){
                card.color = Card.GREEN;
                break;
            } else if(color.equals("B")){
                card.color = Card.BLUE;
                break;
            }
        }
        Server.Discard = card;
        this.cards.remove(card);
        this.sendAll(card.toString());
        if(Server.order==Server.orderList.size()-1){
            Server.order = 1;
        } else if(Server.order==Server.orderList.size()-2) {
            Server.order = 0;
        } else {
            Server.order +=2;
        }
        if(Server.orderList.indexOf(this)==Server.orderList.size()-1){
            this.drawCards(4,Server.orderList.get(0));
        } else {
            this.drawCards(4,Server.orderList.get(Server.orderList.indexOf(this)+1));
        }

    }

    /**
     * play a Skip card
     * @require card != null && card.Type == Skip && card.color == Server.Discard.color
     */
    public void Skip(Card card) throws WrongCardException{
        if (!card.color.equals(Server.Discard.color)  && !Server.Discard.color.equals(Card.WILD) && !Server.Discard.type.equals(Type.Skip)){
            throw new WrongCardException("Invalid action");
        }
        Server.Discard = card;
        this.cards.remove(card);
        this.sendAll(card.toString());
        if(Server.order==Server.orderList.size()-2){
            Server.order = 0;
        } else if (Server.order==Server.orderList.size()-1) {
            Server.order = 1;
        } else {
            Server.order+=2;
        }
    }

    /**
     * play a Draw 2 card
     * @require card != null && card.Type == Draw2 && card.color == Server.Discard.color
     */
    public void playDraw2(Card card) throws WrongCardException{
        if (!card.color.equals(Server.Discard.color)  && !Server.Discard.color.equals(Card.WILD) && !Server.Discard.type.equals(Type.Draw2)){
            throw new WrongCardException("Invalid action");
        }
        Server.Discard = card;
        this.cards.remove(card);
        this.sendAll(card.toString());
        if(Server.order==Server.orderList.size()-1){
            Server.order = 0;
        } else {
            Server.order++;
        }
        if(Server.orderList.indexOf(this)==Server.orderList.size()-1){
            this.drawCards(2,Server.orderList.get(0));
        } else {
            this.drawCards(2,Server.orderList.get(Server.orderList.indexOf(this)+1));
        }
    }

    public boolean checkWin(){
        return cards.size()==0;
    }

    /**
     * draw a card
     * @ensure this.cardList.size() + 1
     */
    public void DrawOneCard(){
        if(Server.order != Server.orderList.indexOf(this)){
            this.send("not your turn");
            return ;
        }
        if(Server.order==Server.orderList.size()-1){
            Server.order = 0;
        } else {
            Server.order++;
        }
        this.drawCards(1,this);
        this.send(Card.printCards(this.cards));
        Server.orderList.get(Server.order).send("Your turn");
        Server.orderList.get(Server.order).send(Card.printCards(Server.orderList.get(Server.order).cards));
    }

    public void restart(){
        Server.shuffle();
        for (PlayerHandler player : Server.orderList){
            player.start=true;
            player.send("Game start");
            player.send(Server.Discard.toString());
            player.send(Card.printCards(player.cards));
        }
        Server.orderList.get(0).send("Your turn");
    }

    /**
     * @require arr != null && arr.length >=2
     */
    public void Chat(String[] arr){
        String chat = "";
        for(int k = 1 ; k< arr.length ; k++){
            chat += arr[k];
            chat+=" ";
        }
        this.sendOthers(this.name+": "+chat);
    }

    @Override
    public void run() {
        outerLoop : while (true){
            try {
                String name = dis.readUTF();
                this.name = name;
                if(StringUtils.isBlank(name)){
                    // if name is blank, ask player to reenter a name
                    this.send("this name is blank");
                    this.send("What's your name?");
                    continue ;
                }
                for(PlayerHandler playerHandler : Server.list){
                    if(playerHandler.name.equals(this.name) && !playerHandler.equals(this) ){
                        // if name is in use, ask player to reenter a name
                        this.send("this name is in use");
                        this.send("What's your name?");
                        continue outerLoop;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.send("joined the game");
            break;
        }
        while (flag){
            sendOther();
            if(!start){
                send("Waiting for host to start");
            }
        }
    }
}
