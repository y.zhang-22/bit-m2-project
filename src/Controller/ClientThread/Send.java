package Controller.ClientThread;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Send implements Runnable{
    private BufferedReader br;
    private DataOutputStream dos;
    private boolean flag = true;

    public Send (Socket client){
        br = new BufferedReader(new InputStreamReader(System.in));
        try {
            dos = new DataOutputStream(client.getOutputStream());
        } catch (IOException e) {
            flag = false;
            try {
                client.close();
                dos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    private String getMessage(){
        String str = "";
        try {
            str = br.readLine();
        } catch (IOException e) {
            flag = false;
            try {
                dos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        return str;
    }

    public void send (String str){
        try {
            dos.writeUTF(str);
            dos.flush();
        } catch (IOException e) {
            flag = false;
            try {
                dos.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    //keep sending messages
    @Override
    public void run() {
        while (flag){
            this.send(getMessage());
            }
        }
    }

