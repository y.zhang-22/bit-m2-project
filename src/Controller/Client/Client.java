package Controller.Client;

import Controller.ClientThread.Receive;
import Controller.ClientThread.Send;
import Model.ComputerGame.ComputerGame;
import Model.Server.Server;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public void joinGame() {
        while (true) {
            System.out.println("Would you like to play with a computer player or join a human game? (AI or Human)");
            Scanner scanner = new Scanner(System.in);
            String str = scanner.next();
            if (str.equals("AI")) {
                playWithComputer();
                break;
            } else if (str.equals("Human")) {
                if(Server.list.size()>10){
                    System.out.println("Sorry, the game is full.");
                    return;
                }
                System.out.println("What's your name?");
                scanner = new Scanner(System.in);
                String name = scanner.nextLine();
                Socket client = null;
                try {
                    client = new Socket("localhost", 9999);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Send send = new Send(client);
                send.send(name);

                Receive receive = new Receive(client);

                //start a receive and send thread to keeping receiving and sending message
                new Thread(send).start();

                new Thread(receive).start();

                break;
            } else {
                System.out.println("wrong command");
            }
        }
    }


    public void playWithComputer() {
        ComputerGame computerGame = new ComputerGame();

        computerGame.Start();
    }
}
