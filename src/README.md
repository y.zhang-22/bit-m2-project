##**About The Project:**

This project implements a UNO game that allows 2 to 10 players to play through server-client or one-on-one with a computer player.

##**Instructions:**

To start the game, the Game Class in the Game package should be run first. Then, run the Player Class, player can choose play against human players or a computer player.

![img.png](imgs/img10.png)

And, players should enter their names.
![img_1.png](imgs/img_1.png)

The first player joins the game is the host of the game, he/she can enter command to start the game.
![img_2.png](imgs/img_2.png)

After a round is over, a new round starts, until there is a winner.
![img_3.png](imgs/img_3.png)

##**Command**:
![img_1.png](imgs/img_6.png)

help ---- print game rules and commands information

------------------------------------------------------------------
![img.png](imgs/img.png)

play n ---- play the card with index n(start from 1)

------------------------------------------------------------------
![img_4.png](imgs/img_9.png)

score ---- print all players' scores

------------------------------------------------------------------
![img_3.png](imgs/img_8.png)

chat **** ---- send message to other players

------------------------------------------------------------------
![img_2.png](imgs/img_7.png)

draw ---- draw a card