package Exception;

public class WrongCardException extends Exception{
    public WrongCardException(String message){
        super(message);
    }
}
