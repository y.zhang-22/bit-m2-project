package Test;

import View.Card;
import Model.Server.Server;
import View.Type;
import org.junit.jupiter.api.Test;
import Model.Server.PlayerHandler;
import Exception.WrongCardException;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import static org.junit.jupiter.api.Assertions.*;

public class ServerTest {
    @Test
    void testShuffle(){
        Server.shuffle();
        assertEquals(107-Server.list.size()*7,Server.cardList.size());
        assertNotNull(Server.Discard);
        PlayerHandler playerHandler = new PlayerHandler();
        Server.list.add(playerHandler);
        Server.shuffle();
        assertEquals(7,playerHandler.cards.size());
    }
}
