package Test;

import Model.ComputerGame.ComputerGame;
import Model.ComputerGame.ComputerPlayer;
import View.Card;
import Model.Server.Server;
import View.Type;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import Model.Server.PlayerHandler;
import Exception.WrongCardException;
import org.junit.runners.MethodSorters;

import static org.junit.jupiter.api.Assertions.*;

public class ComputerGameTest {
    @Test
    void testNewGame(){
        ComputerGame computerGame = new ComputerGame();
        assertEquals(107,computerGame.getCards().size());
    }

    @Test
    void testPlayCard(){
        ComputerGame computerGame = new ComputerGame();
        ComputerPlayer computerPlayer = new ComputerPlayer();
        computerPlayer.addCards(new Card(Type.Num,Card.RED,1));
        computerPlayer.addCards(new Card(Type.Num,Card.BLUE,1));
        computerGame.setComputerPlayer(computerPlayer);
        computerGame.setDiscard(new Card(Type.Num,Card.RED,2));
        assertEquals(2, computerGame.getDiscard().Num);
        computerGame.CPMovement();
        assertEquals(1, computerGame.getDiscard().Num);
        computerGame.CPMovement();
        assertEquals(1, computerGame.getDiscard().Num);

    }
}
