package Test;

import View.Card;
import Model.Server.Server;
import View.Type;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import Model.Server.PlayerHandler;
import Exception.WrongCardException;
import org.junit.runners.MethodSorters;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@FixMethodOrder(MethodSorters.JVM)
public class PlayerHandlerTest {
    static PlayerHandler playerHandler1;
    static PlayerHandler playerHandler2;
    static PlayerHandler playerHandler3;
    static Socket client1;
    static Socket client2;
    static Socket client3;

    static ServerSocket server;

    @BeforeAll
    static void setup() throws IOException {
        server = new ServerSocket(8080);

        client1 = new Socket("localhost", 8080);
        Socket clientP1 = server.accept();
        playerHandler1 = new PlayerHandler(clientP1);

        client2 = new Socket("localhost", 8080);
        Socket clientP2 = server.accept();
        playerHandler2 = new PlayerHandler(clientP2);

        client3 = new Socket("localhost", 8080);
        Socket clientP3 = server.accept();
        playerHandler3 = new PlayerHandler(clientP3);

        Server.list.add(playerHandler1);
        Server.list.add(playerHandler2);
        Server.list.add(playerHandler3);


    }

    @Test
    void testReverse() throws WrongCardException, IOException {
        Server.shuffle();
        Server.orderList = new ArrayList<>();
        Server.orderList.add(playerHandler1);
        Server.orderList.add(playerHandler2);
        Server.orderList.add(playerHandler3);
        playerHandler1.reverse(new Card(Type.Reverse,Server.Discard.color));
        assertEquals(playerHandler3,Server.orderList.get(0));
        assertEquals(playerHandler2,Server.orderList.get(1));
        assertEquals(playerHandler1,Server.orderList.get(2));

    }

    @Test
    void testDraw2() throws IOException, WrongCardException {
        Server.shuffle();
        Server.orderList = new ArrayList<>();
        Server.orderList.add(playerHandler1);
        Server.orderList.add(playerHandler2);
        Server.orderList.add(playerHandler3);
        playerHandler1.playDraw2(new Card(Type.Draw2,Server.Discard.color));
        assertEquals(9,playerHandler2.cards.size());

    }

    @Test
    void testSkip() throws IOException, WrongCardException {
        Server.shuffle();

        Server.order = 0;
        assertEquals(0,Server.order);

        playerHandler1.Skip(new Card(Type.Skip,Server.Discard.color));

        assertEquals(2,Server.order);

    }


    @Test
    void testDrawCard() throws IOException {

        Server.shuffle();

        int card = playerHandler1.cards.size();

        Server.orderList = new ArrayList<>();
        Server.orderList.add(playerHandler1);
        Server.orderList.add(playerHandler2);
        Server.orderList.add(playerHandler3);
        Server.order = Server.orderList.indexOf(playerHandler1);
        playerHandler1.DrawOneCard();

        assertEquals(card+1,playerHandler1.cards.size());

        Server.order = 1;
        playerHandler3.DrawOneCard();

        assertEquals(7,playerHandler3.cards.size());


    }

    @Test
    void checkWin(){
        Server.shuffle();
        assertFalse(playerHandler1.checkWin());
        playerHandler1.cards.clear();
        assertTrue(playerHandler1.checkWin());
    }
}
