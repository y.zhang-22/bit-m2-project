package Test;

import Model.ComputerGame.ComputerPlayer;
import View.Card;
import Model.Server.Server;
import View.Type;
import org.junit.FixMethodOrder;
import org.junit.jupiter.api.*;
import Model.Server.PlayerHandler;
import Exception.WrongCardException;
import org.junit.runners.MethodSorters;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class ComputerPlayerTest {
    @Test
    void testValidMovement(){
        ComputerPlayer cp = new ComputerPlayer();
        cp.addCards(new Card(Type.WildDraw4,Card.WILD));
        cp.addCards(new Card(Type.Num,Card.RED,1));
        Card card = cp.validMovement(new Card(Type.Num,Card.RED,2));
        assertEquals(Type.WildDraw4,card.type);

        card = cp.validMovement(new Card(Type.Num,Card.RED,2));
        assertEquals(Type.Num,card.type);

    }

    @Test
    void testRemoveCard(){
        ComputerPlayer cp = new ComputerPlayer();
        cp.addCards(new Card(Type.WildDraw4,Card.WILD));
        cp.addCards(new Card(Type.Num,Card.RED,1));
        cp.removeCard(0);
        assertEquals(1,cp.AICardList.size());
    }
}
